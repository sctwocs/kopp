// GLOBAL ROUTER OPTIONS
Router.configure({
	layoutTemplate: 'ApplicationLayout',
	loadingTemplate: 'loading',
	notFoundTemplate: 'notFound',
	accessDeniedTemplate: 'accessDenied',
	loginRequiredTemplate: 'loginRequired',
	waitOn: function() {
		return [Meteor.subscribe('cities'), Meteor.subscribe('adCategories')];
	}
});
// END GLOBAL ROUTER OPTIONS

// ROUTE FOR LANDING PAGE
Router.route('/', {
	name: 'home',
	controller: 'HomeController'
});

// ROUTE FOR SEARCH BY LOCATION PAGE
Router.route('/search/:location', {
	name: 'search',
	controller: 'SearchController'
});

// ROUTE FOR SEARCH BY LOCATION AND CATEGORY PAGE
Router.route('/search/:location/:category', {
	name: 'searchByCategory',
	controller: 'SearchByCategoryController'
});

// ROUTE FOR VIEWING A SINGLE AD
Router.route('/ads/:_id', {
	name: 'singleAdView',
	waitOn: function() {
		return [
			Meteor.subscribe('adWithAllImages', this.params._id)
		];
	},
	data: function() { 
		return Ads.findOne(this.params._id); 
	}
});

// ROUTE FOR EDITING A SINGLE AD
Router.route('/ads/:_id/edit', {
	name: 'editAd',
	controller: 'EditAdController'
});

// ROUTE FOR POSTING AN AD
Router.route('/post_ad', {
	name: 'postAd',
	waitOn: function() {
	}
});

// ROUTE FOR UPLOADING IMAGES
Router.route('/upload_images/:_id', {
	name: 'uploadImages',
	waitOn: function() {
		return [
			Meteor.subscribe('singleAd', this.params._id),
			Meteor.subscribe('adImages', this.params._id)
		];
	},
	data: function() { 
		return Ads.findOne(this.params._id); 
	},
	onBeforeAction: function() {
		var ad = Ads.findOne(this.params._id);
		if(isOwner(ad.postedBy)) {
			this.next();
		} else {
			this.render('accessDenied');
		}
	}
});

// ROUTE FOR PREVIEWING THE AD (NOT IMPLEMENTED FULLY)
Router.route('/ad_preview/:_id', {
	name: 'adPreview',
	waitOn: function() {
		return [
			Meteor.subscribe('singleAd', this.params._id),
			Meteor.subscribe('adImages', this.params._id)
		];
	},
	data: function() { 
		return Ads.findOne(this.params._id); 
	}
});

// ROUTE DEFINITION FOR THE USER PROFILE
Router.route("/profile/:_id", {
	name: "profile",
	controller: "ProfileController"
});

// ROUTE DEFINITION FOR CREATE/EDIT PROFILE (for incomplete profiles, may also be used for profile edits)
Router.route("/create_profile/:_id", {
	name: "createProfile",
	controller: "CreateProfileController",
	// only allow users to access this page if the id of the currently logged in user matches the id passed in the url.
	onBeforeAction: function() {
		if(this.params._id != Meteor.userId()) {
			this.render('accessDenied');
		} else {
			this.next();
		}
	}
});

// ROUTE DEFINITION FOR MANAGING ADS
Router.route("/ad_manager", {
	name: "adManager",
	controller: "AdManagerController"
});

// ROUTE DEFINITION FOR MAIN CHAT
Router.route("/chat", {
	name: "chat",
	waitOn: function() {
		return [
			Meteor.subscribe('onlineUsers'),
			Meteor.subscribe('chatrooms')
		];
	}
});

// Routes the user to an access denied page
// if he is not logged in.
var requireLogin = function() {
	if(!Meteor.user()) {
		if(Meteor.loggingIn()) {
			this.render(this.loadingTemplate);
		} else {
			this.render('loginRequired');
		}
	} else {
		this.next();
	}
}

// WE SHOULD PROBABLY BE DOING THIS AT THE LEVEL OF A PUBLICATION
// Returns true if the user is the owner of a particular document. This is only intended for 
// redirecting users. It does not guarantee that documents will not be manipulated from the console.
// Specify allow/deny rules to achieve this.
// NOTE: ALL DOCUMENTS ARE STILL PUBLISHED
// Perhaps do some extra validation at the level of the publication.
var isOwner = function(authorId) {
	currentUser = Meteor.userId();
	if(currentUser === authorId) {
		return true;
	}
	return false;
}

Router.onBeforeAction(requireLogin, {only: ['postAd', 'profile', 'uploadImages', 'adManager', 'chat']});

Router.onBeforeAction('dataNotFound', {only: ['uploadImages', 'singleAdView']});







// NOTES

// only allow users to access this page if the id of the currently logged in user matches the id passed in the url.
// onBeforeAction: function() {
// 	if(this.params._id != Meteor.userId()) {
// 		this.render('accessDenied');
// 	} else {
// 		this.next();
// 	}
// },


// TESTING
Router.route("/cart", {
  name : "cart"
});

// PAYMENT OPTIONS TEST
Router.route("/signup", {
	name: "signup"
});

