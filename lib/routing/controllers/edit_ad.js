EditAdController = RouteController.extend({
	template: "editAd",
	waitOn: function() {
		// // Subscribe to ads belonging to the currently logged in user
		return Meteor.subscribe("singleAd", this.params._id);
	},
	data: function() {
		return Ads.findOne({_id: this.params._id});
	},
	// onBeforeAction: function() {

	// }
});