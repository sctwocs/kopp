CreateProfileController = RouteController.extend({
	template: "createProfile",
	waitOn: function() {
		return [Meteor.subscribe("userProfile", this.params._id)];
	},
	data: function() {
		var id = this.params._id;
		return Meteor.users.findOne({_id:id});
	}
})