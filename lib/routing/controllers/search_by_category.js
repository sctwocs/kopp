SearchByCategoryController = RouteController.extend({
	template: "search",
	waitOn: function() {
		var location = Cities.findOne({slug: this.params.location});
		var category = AdCategories.findOne({slug: this.params.category});
		if(location && category) {
			return Meteor.subscribe("adsWithSingleImageByCategory", location._id, category._id, this.params.query.search);
		} else {
			return;
		}
	},
	data: function() {
		return {
			ads: Ads.find(),
			city: Cities.findOne({slug: this.params.location}),
			adCategories: AdCategories.find() 
		}
	},
	onBeforeAction: function() {
		if(!Cities.findOne({slug: this.params.location}) || !AdCategories.findOne({slug: this.params.category})) {
			this.render('notFound');
		} else {
			this.next();
		}
	}
});	