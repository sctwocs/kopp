// We are only subscribing to the dataset belonging to the logged in user.
// Assuming the logged in user cannot mimic another user's id, he should
// not be able to view posts by other users.
// Additional allow/deny permissions will be set to prevent user's from 
// manipulating the documents that are not their own.

// NOTE: Can someone impersonate another user by updating his own id? Are updates
// to the id field denied by default? Research this.

AdManagerController = RouteController.extend({
	template: "adManager",
	waitOn: function() {
		// Subscribe to ads belonging to the currently logged in user
		return Meteor.subscribe('myAds');
	},
	data: function() {
		return {
			ads: Ads.find(),
		}
	}
});