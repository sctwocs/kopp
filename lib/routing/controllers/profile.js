ProfileController = RouteController.extend({
	template: "profile",
	waitOn: function() {
		return [Meteor.subscribe("userProfile", this.params._id), 
				Meteor.subscribe("userAds", this.params._id), 
				Meteor.subscribe('userReviews', this.params._id),
				]
	},
	data: function() {
		var id = this.params._id;
		return {
			profileData: Meteor.users.findOne({_id:id}),
			adsData: Ads.find({postedBy:id})
		}
	}
});