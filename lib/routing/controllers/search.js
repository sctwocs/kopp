SearchController = RouteController.extend({
	template: "search",
	waitOn: function() {
		var location = Cities.findOne({slug: this.params.location});

		if(location) {
			return Meteor.subscribe("adsWithSingleImage", location._id, this.params.query.search);
		}
	},
	data: function() {
		return {
			ads: Ads.find(),
			city: Cities.findOne({slug: this.params.location}),
			adCategories: AdCategories.find() 
		}
	},
	onBeforeAction: function() {
		if(!Cities.findOne({slug: this.params.location})) {
			this.render('notFound');
		} else {
			this.next();
		}
	}
});	