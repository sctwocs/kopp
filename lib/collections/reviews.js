Reviews = new Mongo.Collection('reviews');

Meteor.methods({
	addReview: function(reviewAttributes) {
		check(Meteor.userId(), String);
		check(reviewAttributes, {
			rating: Number,
			comment: String,
			postedTo: String
		});

		var reviews = Reviews.find().fetch();

		var user = Meteor.user();

		var review = _.extend(reviewAttributes, {
			postedOn: new Date(),
			postedBy: user._id,
			posterEmail: user.emails[0].address
		});

		var reviewId = Reviews.insert(review);

		return {
			_id: reviewId
		};
	}
});

// Schemas.Review = new SimpleSchema({
// 	rating: {
// 		type: Number
// 	},
// 	comment: {
// 		type: String,
// 		min: 1,
// 		max: 2000,
// 		autoform: {
// 			rows: 6
// 		}
// 	},
// 	postedOn: {
// 		type: Date,
// 		autoValue: function() {
// 			if(this.isInsert) {
// 				return new Date();
// 			}
// 		}		
// 	},
// 	postedFor: {
// 		type: String
// 	},
// 	postedBy: {
// 		type: String,
// 		label: 'Posted by',
// 		autoValue: function() {
// 			if(this.isInsert) {
// 				return Meteor.userId();
// 			}
// 		}
// 	},
// 	posterEmail: {
// 		type: String,
// 		label: 'Email',
// 		autoValue: function() {
// 			if(this.isInsert) {
// 				return Meteor.user().emails[0].address;
// 			}
// 		}
// 	}
// }); 

// Reviews.attachSchema(Schemas.Review);