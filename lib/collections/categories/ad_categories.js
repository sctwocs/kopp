Schemas = {};

AdCategories = new Mongo.Collection('ad-categories');

Schemas.AdCategory = new SimpleSchema({
	name: {
		type: String,
		label: 'Category',
		min: 1,
		max: 200,
		unique: true
	},
	icon: {
		type: String,
		optional: true
	},
	labelEn: {
		type: String,
		label: 'Label (English)',
		min: 1,
		max: 200
	},
	parent: {
		type: String,
		optional: true
	},
	slug: {
		type: String,
		unique: true,
		optional: true
	},
	ancestors: {
		type: [Object],
		index: 1,
		optional: true,
		minCount: 0
	},
	"ancestors.$._id": {
		type: String
	},
	"ancestors.$.slug": {
		type: String,
		min: 1,
		max: 200
	},
	"ancestors.$.name": {
		type: String,
		min: 1,
		max: 200
	}
});

AdCategories.attachSchema(Schemas.AdCategory);