var createProfileImageThumb = function(fileObj, readStream, writeStream) {
	var size = '250';
	gm(readStream).autoOrient().resize(size, size + '^').gravity('Center').extent(size, size).stream('PNG').pipe(writeStream);
};

ProfileImageUploads = new FS.Collection('profileImageUploads', {
	stores: [
		new FS.Store.FileSystem('profile_image_uploads'),
		new FS.Store.FileSystem('profile_image_thumbs', {transformWrite: createProfileImageThumb})
	],
	filter: {
		maxSize: 10000000,
		allow: {
			contentTypes: ['image/*'],
			extensions: ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG']
		}
	}
});

if(Meteor.isServer) {
	ProfileImageUploads.allow({
		'insert': function() {
			return true;
		},
		'update': function() {
			return true;
		},
		'download': function() {
			return true;
		},
		'remove': function() {
			return true;
		}
	});
}