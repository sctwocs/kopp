// even if insecure package is removed, profile details
// are editable by default,
// can no longer make changes to profile through console, the cmd below will no longer work
// cmd: Meteor.users.update(Meteor.userId(), {$set: {'profile.isAdmin': true}}); 
Meteor.users.deny({
    update: function() {
        return true;
    }
});

// METHODS
// Create/Update Profile
Meteor.methods({
    updateProfile: function(doc) {
        // make sure the user is logged in
        if(!Meteor.userId()) {
            throw new Meteor.Error("not-authorized");
        }

        // should we have a check here to make sure that the currently logged in user
        // is the one updating the profile? 
        
        // server-side validation, will throw error if check against schema fails
        check(doc, Schemas.UserProfile);

        var profileData = doc.$set;

        // update the currently logged in users information
        Meteor.users.update({_id: Meteor.userId()}, {$set: {profile: profileData}});

        // Meteor.users.update({_id: Meteor.userId()}, doc)

        // Router.go is a client side method, wrap with Meteor.isClient or will throw
        // internal server error server side
        if(Meteor.isClient) {
            Router.go('profile', { _id: Meteor.userId() });
        }

        // skeleton of a method
        // a) basic checks
        // b) rate limiting
        // c) properties check
        // d) call updateProfile function
    }
});


Schemas.UserProfile = new SimpleSchema({
	firstName: {
		type: String,
		regEx: /^[a-zA-Z-]{2,25}$/,
        min: 1,
        max: 100
	},
	lastName: {
		type: String,
		regEx: /^[a-zA-Z-]{2,25}$/,
        min: 1,
        max: 100
	},
	birthday: {
		type: Date,
		optional: true,
        autoform: {
            type: "bootstrap-datepicker"
        }
	},
	gender: {
		type: String,
		allowedValues: ['Male', 'Female'],
		optional: true
	},
    // picture: {
    //     type: String,
    //     autoform: {
    //         afFieldInput: {
    //             type: 'fileUpload',
    //             collection: 'Images',   // field name of images collectionFS
    //             label: 'Choose file'
    //         }
    //     }
    // }
});



Schemas.User = new SimpleSchema({
    // username: {
    //     type: String,
    //     regEx: /^[a-z0-9A-Z_]{3,15}$/,
    //     optional: true
    // },
    emails: {
        type: [Object],
        // this must be optional if you also use other login services like facebook,
        // but if you use only accounts-password, then it can be required
        optional: true
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Schemas.UserProfile,
        blackbox: true,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Add `roles` to your schema if you use the meteor-roles package.
    // Option 1: Object type
    // If you specify that type as Object, you must also specify the
    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
    // Example:
    // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
    // You can't mix and match adding with and without a group since
    // you will fail validation in some cases.
    roles: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true
    },
    status: {
    	type: Object,
    	blackbox: true,	// blackbox true option skips validation for everything within the object.
    	optional: true
    }
});

// Collection helpers for users collection
Meteor.users.helpers({
    profileImages: function() {
        return ProfileImageUploads.find({ 'metadata.owner': this._id });
    }
});

Meteor.users.attachSchema(Schemas.User);