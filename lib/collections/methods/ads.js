// METHODS
if(Meteor.isServer) {
	Meteor.methods({
		deleteAd: function(adId) {
			// data integrity
			check(adId, String);
			// find ad to be deleted
			ad = Ads.findOne(adId);

			// ensure that the currently logged in user is the owner of this ad
			if(ad.postedBy !== Meteor.userId()) {
				throw new Meteor.Error("not-authorized.");
			}
			// remove ad
			Ads.remove(adId);
			// remove associated images
			Uploads.remove({'metadata.adId' : ad._id})
		}
	});
}
