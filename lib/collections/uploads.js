var createThumb = function(fileObj, readStream, writeStream) {
	var size = '250';
	gm(readStream).autoOrient().resize(size, size + '^').gravity('Center').extent(size, size).stream('PNG').pipe(writeStream);
};


// To be secure, this must be added on the server. However, you should use the filter option 
// on the client, too, to help catch many of the disallowed uploads there and allow you to display 
// a helpful message with your onInvalid function.
Uploads = new FS.Collection('uploads', {
	stores: [
		new FS.Store.FileSystem('uploads'),
		new FS.Store.FileSystem('thumbs', {transformWrite: createThumb})
	],
	filter: {
		maxSize: 10000000,
		allow: {
			contentTypes: ['image/*'],
			extensions: ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG']
		}
	}
});

if(Meteor.isServer) {
	Uploads.allow({
		'insert': function() {
			return true;
		},
		'update': function() {
			return true;
		},
		'download': function() {
			return true;
		},
		'remove': function() {
			return true;
		}
	});
}