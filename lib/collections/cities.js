Cities = new Mongo.Collection('cities');

Schemas.City = new SimpleSchema({
	name: {	
		type: String,
		label: 'City',
		min: 1,
		max: 200,
		autoform: {
			options: function() {
				var options = [];
				Cities.find().forEach(function(element) {
					options.push({
						label: element.name, value: element._id
					})
				});
				return options;
			}
		}
	}, 
	slug: {
		type: String,
		min: 1,
		max: 500
	}
});

Cities.attachSchema(Schemas.City);