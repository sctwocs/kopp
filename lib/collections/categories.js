Categories = new Mongo.Collection('categories');

Schemas.Category = new SimpleSchema({
	name: {
		type: String,
		label: 'Category',
		min: 1,
		max: 200,
		unique: true
	},
	icon: {
		type: String,
		optional: true
	},
	children: {
		type: [Object],
		index: 1,
		minCount: 0
	},
	"children.$.name": {
		type: String,
		min: 1,
		max: 200
	} 
});

Categories.attachSchema(Schemas.Category);

// The query to retrieve the immediate children of a 
// node is fast and straightforward:
// db.categories.findOne({_id: "ParentId"}).children
