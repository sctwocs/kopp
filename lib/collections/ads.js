Ads = new Mongo.Collection('ads');

// ALLOW/DENY RULES

// SECURITY: https://www.discovermeteor.com/blog/allow-deny-a-security-primer/
// SECURITY: http://joshowens.me/meteor-security-101/
// Prevent users from updating the documents owner

// SCHEMA
Schemas.Ad = new SimpleSchema({
	adType: {
		type: String,
		label: 'Ad Type',
		allowedValues: ['offering', 'seeking'],
		autoform: {
			options: [
				{label: "I am offering an item for purchase.", value: "offering"},
				{label: "I am seeking to purchase an item.", value: "seeking"}
			]
		}
	},
	location: {
		type: String,
		label: 'Location',
		min: 1,
		max: 200,
		autoform: {
			options: function() {
				var options = [];
				Cities.find().forEach(function(element) {
					options.push({
						label: element.name, value: element._id
					})
				});
				return options;
			}
		}
	},
	masterCategory: {
		type: String,
		label: 'Category',
		min: 1,
		max: 200,
		optional: true,
		autoform: {
			options: function() {
				var options = [];
				AdCategories.find().forEach(function(element) {
					if(!element.parent || element.parent == "") {
						options.push({
							label: element.labelEn, value: element._id
						})
					}
				});
				return options;
			}
		}
	},
	subCategory: {
		type: String,
		label: 'Subcategory',
		min: 1,
		max: 200,
		optional: true, 
		autoform: {
			type: 'select',
			options: function() {
				var options = [];
				if(Meteor.isClient) {
					// Quick and dirty solution
					categoryId = AutoForm.getFieldValue('masterCategory', 'generalAdInfo') || AutoForm.getFieldValue('masterCategory', 'updateGeneralAdInfo');
					if(categoryId) {
						AdCategories.find().forEach(function(element) {
							if(element.parent == categoryId) {
								options.push({
									label: element.labelEn, value: element._id
								})
							}
						});
						return options;
					}
				}
			}
		}
	},
	title: {
		type: String,
		label: 'Ad Title',
		min: 1,
		max: 200
	},
	price: {
		type: Number,
		label: 'Price (USD)',
		min: 0,
		max: 999999
	},
	description: {
		type: String,
		label: 'Description',
		// min: 20,					/// initially the WYSIWYG description input field generates markup, therefore field can be inadvertently left "empty"
		// max: 1000,
		autoform: {
			rows: 10,
			class: "editor"
		}
	},
	postedOn: {
		type: Date,
		label: 'Date',
		autoValue: function() {
			if(this.isInsert) {
				return new Date();
			}
		}
	},
	postedBy: {
		type: String,
		label: 'Posted by',
		autoValue: function() {
			if(this.isInsert) {
				return Meteor.userId();
			}
		}
	},
	posterEmail: {
		type: String,
		label: 'Email',
		autoValue: function() {
			if(this.isInsert) {
				return Meteor.user().emails[0].address;
			}
		}
	}
});

Ads.attachSchema(Schemas.Ad);

Ads.helpers({
	images: function() {
		return Uploads.find({ 'metadata.adId': this._id });
	}
});

if(Meteor.isServer) {
	Ads._ensureIndex({
		"title": "text",
		"description": "text"
	});
}