// AdminConfig = {
//   name: 'KOPP DASHBOARD',
//   adminEmails: ['sctwocs@gmail.com', '1234@gmail.com'],
//   collections: {
//   	AdCategories: {
//   		label: 'Ad Categories',
//   		icon: 'star',
//   		tableColumns: [
//   			{label: 'Category', name: 'name'},
//   			{label: 'Parent', name: 'parent'},
//   			{label: 'Slug', name: 'slug'}
//   		]
//   	},
//   	Categories: {
//   		label: 'Legacy Categories',
//   		icon: 'star',
//   		tableColumns: [
//   			{label: 'Category', name: 'name'},
//   			{label: 'Children', name: 'children'}
//   		]
//   	},
//     Ads: {
//       label: 'Ads',
//       icon: 'star',
//       tableColumns: [
//         {label: 'Title', name: 'title'},
//         {label: 'Description', name: 'description'},
//         {label: 'Posted by', name: 'posterEmail'},
//         {label: 'Posted on', name: 'postedOn'}
//       ]
//     }
//   }
// };