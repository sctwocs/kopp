Accounts.onCreateUser(function(options, user) {
	// all newly created users are administrators for debugging purposes.
	user.roles = ["standard-user", "admin"];
	if(options.profile)
		user.profile = options.profile;
	return user;
});