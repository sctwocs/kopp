// ----------------------------------------------------------------------//
//	PUBLISH ENTIRE DATASET FOR A PARTICULAR COLLECTION
// ----------------------------------------------------------------------//

// Publish all legacy categories. Currently we are using ad categories as the
// primary dataset for organizing categories.
Meteor.publish('categories', function() {
	return Categories.find();
});

// This is our primary collection for ad categories. It uses the ancestors
// pattern to model the category tree.
Meteor.publish('adCategories', function() {
	return AdCategories.find();
});

// This is our primary cities collection. 
Meteor.publish('cities', function() {
	return Cities.find();
});

// This is our primary collection for ad images.
Meteor.publish('uploads', function() {
	return Uploads.find(); 
});

// This is our primary collection for all ads.
Meteor.publish('ads', function() {
	return Ads.find();
});

// This is our primary collection for messages
Meteor.publish('messages', function() {
	return Messages.find();
});

// This is our primary collection for ratings
Meteor.publish('reviews', function() {
	return Reviews.find();
});

// ----------------------------------------------------------------------//
//	END PUBLISH ENTIRE DATASET FOR A PARTICULAR COLLECTION
// ----------------------------------------------------------------------//



// ----------------------------------------------------------------------//
//	PUBLICATIONS RELATED TO ADS
// ----------------------------------------------------------------------//

// The following method publishes a single ad.
Meteor.publish('singleAd', function(id) {
	check(id, String);
	return Ads.find(id);
});

// This method publishes all ads belonging to a particular user (for instance, 
// in the user profile 'active ads' pane).
Meteor.publish('userAds', function(id){
	check(id, String);
	return Ads.find({postedBy:id}, {sort: {postedOn: -1}});
});

// Publishes the ads belonging to the currently logged in user. Does not require url param.
Meteor.publish('myAds', function() {
	// returns undefined if not logged in.
	if(this.userId) {
		return Ads.find({postedBy: this.userId});
	}
	this.ready();
});

// The following publication returns the images associated with 
// a particular ad (for instance, in the upload images view).
Meteor.publish('adImages', function(id) {
	check(id, String);
	return Uploads.find({'metadata.adId' : id});
});

// Publishes all ads in a given location with one associated image.
Meteor.publishComposite('adsWithSingleImage', function(locationId, searchValue) {
	return {
		find: function() {

			var data;

			check(locationId, String);

			if(!searchValue) {
				// find function must return a cursor containing top level documents
				data = Ads.find({location: locationId}, {sort: {postedOn: -1}});
			} else {
				check(searchValue, String);
				data = Ads.find(
					{ $and: [
						{ $text: { $search: searchValue } },
						{ location: locationId }
						]
					},
					{
						fields: {
							score: { $meta: "textScore" }
						},
						sort: {
							score: { $meta: "textScore" }
						}
					}
				);
			}

			if(data) {
				return data;
			}
			// in the event that a document isn't found
			this.ready();
		},
		children: [
			{
				// the top level document is passed in as an argument (in this case, the top level document is ad)
				// this must return a cursor of second tier documents (in this, case the images associated with the ad)
				find: function(ad) {
					return Uploads.find({'metadata.adId': ad._id}, {limit: 1});
				}
			}
		]
	}
});

// Publishes all ads in a given location AND within a given category with one associated image.
// NOT TESTED YET
Meteor.publishComposite('adsWithSingleImageByCategory', function(locationId, categoryId, searchValue) {
	return {
		find: function() {

			var data;

			check(locationId, String);
			check(categoryId, String);

			if(!searchValue) {
				data = Ads.find(
					{ $and: [
						{ location: locationId }, 
						{ $or: [
							{ masterCategory: categoryId }, 
							{subCategory: categoryId}
							] 
						}
						]
					}, 
					{sort: {postedOn: -1}});
			} else {
				check(searchValue, String);
				data = Ads.find(
					{ $and: [
						{ $text: { $search: searchValue } },
						{ location: locationId },
						{ masterCategory: categoryId }
						]
					},
					{
						fields: {
							score: { $meta: "textScore" }
						},
						sort: {
							score: { $meta: "textScore" }
						}
					}
				);
			}
			
			if(data) {
				return data;
			}
			this.ready();
		},
		children: [
			{
				find: function(ad) {
					return Uploads.find({'metadata.adId' : ad._id}, {limit: 1});
				}
			}
		]
	}
});

// Publishes a single ad with all associated images.
Meteor.publishComposite('adWithAllImages', function(adId) {
	return {
		find: function() {
			return Ads.find({_id: adId});
		},
		children: [
			{
				find: function(ad) {
					return Uploads.find({'metadata.adId': ad._id});
				}
			}
		]
	}
});

// ----------------------------------------------------------------------//
//	END PUBLICATIONS RELATED TO ADS
// ----------------------------------------------------------------------//



// ----------------------------------------------------------------------//
//	PUBLICATIONS FOR USER PROFILE
// ----------------------------------------------------------------------//

// The following publication returns user information. The type of data that
// is returned depends on whether or not the user id passed in is equal to 
// the user id of the currently logged in user.
Meteor.publishComposite("userProfile", function(id) {
	check(id, String);
	// try to find the user
	var user = Meteor.users.findOne({_id:id});
	// if we can't find the user, mark the subscription as ready and quit
	if(!user) {
		this.ready();
		return;
	}
	// if the user we want to display the profile for is the currently logged in user
	if(this.userId == user._id) {
		// then we return the corresponding full document via a cursor
		return {
			find: function() {
				return Meteor.users.find(this.userId);
			},
			children: [
				{
					find: function(user) {
						return ProfileImageUploads.find({'metadata.owner': this.userId}, {limit: 1});
					}
				}
			]
		}
	} 
	else {
		return {
			find: function() {
				return Meteor.users.find(user._id, {fields: {"profile" : 1}});
			},
			children: [
				{
					find: function(user) {
						return ProfileImageUploads.find({'metadata.owner': user._id}, {limit: 1});
					}
				}
			]
		}
	}
});

// PUBLISH REVIEWS POSTED TO A PARTICULAR USERS PROFILE
Meteor.publish('userReviews', function(id){
	check(id, String);
	return Reviews.find({postedTo:id}, {sort: {postedOn: -1}});
});

// ----------------------------------------------------------------------//
//	END PUBLICATIONS FOR USER PROFILE
// ----------------------------------------------------------------------//



// CHATROOM TESTS
Meteor.publish("chatrooms", function() {
	return ChatRooms.find({});
});

Meteor.publish("onlineUsers", function() {
	return Meteor.users.find({"status.online": true}, {fields: { "emails" : 1 } });
});