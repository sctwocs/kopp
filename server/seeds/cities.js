Meteor.startup(function() {
	if(Cities.find().count() === 0) {
		[
			{name: "Bandalungwa", slug: "bandalungwa"},
			{name: "Barumbu", slug: "barumbu"},
			{name: "Bumbu", slug: "bumbu"},
			{name: "Gombe", slug: "gombe"},
			{name: "Kalamu", slug: "kalamu"},
			{name: "Kasa-vubu", slug: "kasa-vubu"},
			{name: "Kimbanseke", slug: "kimbanseke"},
			{name: "Kinshasa", slug: "kinshasa"},
			{name: "Kintambo", slug: "kintambo"},
			{name: "Kisenso", slug: "kisenso"},
			{name: "Lemba", slug: "lemba"},
			{name: "Limete", slug: "limete"},
			{name: "Lingwala", slug: "lingwala"},
			{name: "Makala", slug: "makala"},
			{name: "Maluku", slug: "maluku"},
			{name: "Masina", slug: "masina"},
			{name: "Matete", slug: "matete"},
			{name: "Mont Ngafula", slug: "mont-ngafula"},
			{name: "N'Djili", slug: "n-djili"},
			{name: "Ngaba", slug: "ngaba"},
			{name: "Ngaliema", slug: "ngaliema"},
			{name: "Ngiri Ngiri", slug: "ngiri-ngiri"},
			{name: "Nsele", slug: "nsele"},
			{name: "Selembo", slug: "selembo"}
		].forEach(function(city) {
			Cities.insert(city);
		});
	}
});