Meteor.startup(function() {
	if(Categories.find().count() === 0) {
		[
			{
				name: "buy and sell",
				icon: "fa fa-usd",
				children: [
					{name: "art, collectibles"}, 
					{name: "baby items"}, 
					{name: "bikes"}, 
					{name: "books"},
					{name: "business, industrial"},
					{name: "cameras, camcorders"},
					{name: "cds, dvds, blu-ray"},
					{name: "clothing"},
					{name: "computers"},
					{name: "computer accessories"},
					{name: "electronics"},
					{name: "furniture"},
					{name: "garage sales"},
					{name: "health, special needs"},
					{name: "hobbies, crafts"},
					{name: "home appliances"},
					{name: "home - indoor"},
					{name: "home - outdoor"},
					{name: "home renovation materials"},
					{name: "iPods, mp3, headphones"},
					{name: "jewellery, watches"},
					{name: "musical instruments"},
					{name: "phones"},
					{name: "sporting goods, exercise"},
					{name: "tickets"},
					{name: "tools"},
					{name: "toys, games"},
					{name: "video games, consoles"},
					{name: "other"}
				]
			},
			{
				name: "services",
				icon: "fa fa-wrench",
				children: [
					{name: "childcare, nanny"},
					{name: "cleaners, cleaning"},
					{name: "entertainment"},
					{name: "financial, legal"},
					{name: "fitness, personal trainer"},
					{name: "health, beauty"},
					{name: "moving, storage"},
					{name: "music lessons"},
					{name: "photography, video"},
					{name: "skilled trades"},
					{name: "tutors, languages"},
					{name: "wedding"},
					{name: "travel, vacations"},
					{name: "other"}
				]
			},
			{
				name: "cars & vehicles",
				icon: "fa fa-car",
				children: [
					{name: "used cars & trucks"},
					{name: "cars"},
					{name: "SUVs"},
					{name: "trucks"},
					{name: "vans"},
					{name: "new cars & trucks"},
					{name: "classic cars"},
					{name: "auto parts, tires"},
					{name: "automotive services"},
					{name: "motorcycles"},
					{name: "ATVs, snowmobiles"},
					{name: "boats, watercraft"},
					{name: "RVs, campers, trailers"},
					{name: "heavy equipment"},
					{name: "other"},			
				]
			},
			{
				name: "pets",
				icon: "fa fa-paw",
				children: [
					{name: "accessories"},
					{name: "animal, pet services"},
					{name: "birds for sale"},
					{name: "cats, kittens for sale"},
					{name: "dogs, puppies for sale"},
					{name: "livestock for sale"},
					{name: "lost & found"},
					{name: "other pets for sale"},
					{name: "other"}
				]
			},
			{
				name: "vacation rentals",
				icon: "fa fa-sun-o",
				children: [
					{name: "Canada"},
					{name: "USA"},
					{name: "Caribbean"},
					{name: "Mexico"},
					{name: "Other Countries"}
				]
			},
			{
				name: "community",
				icon: "fa fa-users",
				children: [
					{name: "activities, groups"},
					{name: "artists, musicians"},
					{name: "classes, lessons"},
					{name: "events"},
					{name: "friendship, networking"},
					{name: "long lost relationships"},
					{name: "lost & found"},
					{name: "missed connections"},
					{name: "rideshare"},
					{name: "volunteers"},
					{name: "other"}
				]
			},
			{
				name: "real estate",
				icon: "fa fa-home",
				children: [
					{name: "apartments, condos for rent"},
					{name: "house rental"},
					{name: "room rental, roommates"},
					{name: "short term rentals"},
					{name: "commercial, office space for rent"},
					{name: "storage, parking for rent"},
					{name: "houses for sale"},
					{name: "condos for sale"},
					{name: "land for sale"},
					{name: "real estate services"},
					{name: "other"}
				]
			},
			{
				name: "jobs",
				icon: "fa fa-briefcase",
				children: [
					{name: "accounting, management"},
					{name: "child care"},
					{name: "bar, food, hospitality"},
					{name: "cleaning, housekeeper"},
					{name: "construction, trades"},
					{name: "customer service"},
					{name: "driver, security"},
					{name: "general labour"},
					{name: "graphic, web design"},
					{name: "healthcare"},
					{name: "hair stylist, salon"},
					{name: "office manager, receptionist"},
					{name: "part time, students"},
					{name: "programmers, computer"},
					{name: "sales, retail sales"},
					{name: "tv, media, fashion"},
					{name: "other"}
				]
			},
			{
				name: "resumes",
				icon: "fa fa-file-text",
				children: [
					{name: "child care"},
					{name: "construction, trades"},
					{name: "general labour"},
					{name: "other"}
				]
			},
			{
				name: "personals",
				icon: "fa fa-heart",
				children: []
			}			
		].forEach(function(category) {
			Categories.insert(category);
		});
	}
});