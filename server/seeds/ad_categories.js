// Category hierarchy, TO COMPLETE
// reference: https://docs.mongodb.org/ecosystem/use-cases/category-hierarchy/

Meteor.startup(function() {

	// remove all categories at startup, force re-seed
	// AdCategories.remove({});

	// add master categories first (we will need to reference the _id of the parent category in the child category)
	if(AdCategories.find().count() === 0) {

		var carsAndVehiclesId = AdCategories.insert({
			name: 'carsAndVehicles',
			icon: 'fa fa-car',
			labelEn: 'Cars and Vehicles',
			parent: '',
			slug: 'cars-vehicles',
			ancestors: []
		});

		var carsAndVehicles = AdCategories.findOne(carsAndVehiclesId);

		var phonesAndTabletsId = AdCategories.insert({
			name: 'phonesAndTablets',
			icon: 'fa fa-mobile',
			labelEn: 'Phones and Tablets',
			parent: '',
			slug: 'phones-tablets',
			ancestors: []
		});

		var phonesAndTablets = AdCategories.findOne(phonesAndTabletsId);

		var realEstateId = AdCategories.insert({
			name: 'realEstate',
			icon: 'fa fa-home',
			labelEn: 'Real Estate',
			parent: '',
			slug: 'real-estate',
			ancestors: []
		});

		var realEstate = AdCategories.findOne(realEstateId);

		var servicesId = AdCategories.insert({
			name: 'services',
			icon: 'fa fa-wrench',
			labelEn: 'Services',
			parent: '',
			slug: 'services',
			ancestors: []
		});

		var services = AdCategories.findOne(servicesId);

		var electronicsId = AdCategories.insert({
			name: 'electronics',
			icon: 'fa fa-laptop',
			labelEn: 'Electronics',
			parent: '',
			slug: 'electronics',
			ancestors: []
		});

		var electronics = AdCategories.findOne(electronicsId);

		var fashionId = AdCategories.insert({
			name: 'fashion',
			icon: 'fa fa-black-tie',
			labelEn: 'Fashion',
			parent: '',
			slug: 'fashion',
			ancestors: []
		});

		var fashion = AdCategories.findOne(fashionId);


		// Insert children
		[
			{
				name: 'carsAndTrucks',
				labelEn: 'Cars and Trucks',
				parent: carsAndVehicles._id,
				slug: 'cars-trucks',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'autoPartsTires',
				labelEn: 'Auto Parts and Tires',
				parent: carsAndVehicles._id,
				slug: 'auto-parts-tires',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'automotiveServices',
				labelEn: 'Automotive Services',
				parent: carsAndVehicles._id,
				slug: 'automotive-services',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'boatsWatercrafts',
				labelEn: 'Boats and Watercrafts',
				parent: carsAndVehicles._id,
				slug: 'boats-watercrafts',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'rvsCampersTrailers',
				labelEn: 'RV\'s, Campers, and Trailers',
				parent: carsAndVehicles._id,
				slug: 'rvs-campers-trailers',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'carsOther',
				labelEn: 'Other',
				parent: carsAndVehicles._id,
				slug: 'cars-other',
				ancestors: [
					{
						_id: carsAndVehicles._id,
						slug: carsAndVehicles.slug,
						name: carsAndVehicles.name
					}
				]
			},
			{
				name: 'cellPhones',
				labelEn: 'Cellphones',
				parent: phonesAndTablets._id,
				slug: 'cellphones',
				ancestors: [
					{
						_id: phonesAndTablets._id,
						slug: phonesAndTablets.slug,
						name: phonesAndTablets.name
					}
				]
			},
			{
				name: 'tablets',
				labelEn: 'Tablets',
				parent: phonesAndTablets._id,
				slug: 'tablets',
				ancestors: [
					{
						_id: phonesAndTablets._id,
						slug: phonesAndTablets.slug,
						name: phonesAndTablets.name
					}
				]				
			},
			{
				name: 'accessories',
				labelEn: 'Accessories',
				parent: phonesAndTablets._id,
				slug: 'accessories',
				ancestors: [
					{
						_id: phonesAndTablets._id,
						slug: phonesAndTablets.slug,
						name: phonesAndTablets.name
					}
				]
			},
			{
				name: 'phonesAndTabletsOther',
				labelEn: 'Other',
				parent: phonesAndTablets._id,
				slug: 'phones-and-tablets-other',
				ancestors: [
					{
						_id: phonesAndTablets._id,
						slug: phonesAndTablets.slug,
						name: phonesAndTablets.name
					}
				]
			},
			{
				name: 'realEstateForLease',
				labelEn: 'For Lease',
				parent: realEstate._id,
				slug: 'real-estate-for-lease',
				ancestors: [
					{
						_id: realEstate._id,
						slug: realEstate.slug,
						name: realEstate.name
					}
				]
			},
			{
				name: 'realEstateForSale',
				labelEn: 'For Sale',
				parent: realEstate._id,
				slug: 'real-estate-for-sale',
				ancestors: [
					{
						_id: realEstate._id,
						slug: realEstate.slug,
						name: realEstate.name
					}
				]
			},
			{
				name: 'realEstateOther',
				labelEn: 'Other',
				parent: realEstate._id,
				slug: 'real-estate-other',
				ancestors: [
					{
						_id: realEstate._id,
						slug: realEstate.slug,
						name: realEstate.name
					}
				]
			},
			{
				name: 'cateringService',
				labelEn: 'Catering Service',
				parent: services._id,
				slug: 'catering-service',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'plumbing',
				labelEn: 'Plumbing',
				parent: services._id,
				slug: 'plumbing-service',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'electricians',
				labelEn: 'Electricians',
				parent: services._id,
				slug: 'electricians',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'drivers',
				labelEn: 'Drivers',
				parent: services._id,
				slug: 'drivers',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'teachers',
				labelEn: 'Teachers',
				parent: services._id,
				slug: 'teachers',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'translators',
				labelEn: 'Translators',
				parent: services._id,
				slug: 'translators',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'taxiServices',
				labelEn: 'Taxi Services',
				parent: services._id,
				slug: 'taxi-services',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'photographers',
				labelEn: 'Photographers',
				parent: services._id,
				slug: 'photographers',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'painters',
				labelEn: 'Painters',
				parent: services._id,
				slug: 'painters',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'builders',
				labelEn: 'Builders',
				parent: services._id,
				slug: 'builders',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'carpenters',
				labelEn: 'Carpenters',
				parent: services._id,
				slug: 'carpenters',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'security',
				labelEn: 'Security',
				parent: services._id,
				slug: 'security',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'funeralServices',
				labelEn: 'Funeral Services',
				parent: services._id,
				slug: 'funeral-services',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'sculptors',
				labelEn: 'Sculptors',
				parent: services._id,
				slug: 'sculptors',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'architects',
				labelEn: 'Architects',
				parent: services._id,
				slug: 'architects',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'veterinarian',
				labelEn: 'Veterinarian',
				parent: services._id,
				slug: 'veterinarian',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'hairdressing',
				labelEn: 'Hairdressing',
				parent: services._id,
				slug: 'hairdressing',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'tailors',
				labelEn: 'Tailors',
				parent: services._id,
				slug: 'tailors',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},	
			{
				name: 'servicesOther',
				labelEn: 'Other',
				parent: services._id,
				slug: 'services-other',
				ancestors: [
					{
						_id: services._id,
						slug: services.slug,
						name: services.name
					}
				]
			},
			{
				name: 'laptops',
				labelEn: 'Laptops',
				parent: electronics._id,
				slug: 'laptops',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},	
			{
				name: 'desktops',
				labelEn: 'Desktops',
				parent: electronics._id,
				slug: 'desktops',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'cookers',
				labelEn: 'Cookers',
				parent: electronics._id,
				slug: 'cookers',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'televisions',
				labelEn: 'Televisions',
				parent: electronics._id,
				slug: 'televisions',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},			
			{
				name: 'dvdCd',
				labelEn: 'DVDs, CDs',
				parent: electronics._id,
				slug: 'dvd-cd',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'amplifiers',
				labelEn: 'Amplifiers',
				parent: electronics._id,
				slug: 'amplifiers',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'speakers',
				labelEn: 'Speakers',
				parent: electronics._id,
				slug: 'speakers',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'lights',
				labelEn: 'Lights',
				parent: electronics._id,
				slug: 'lights',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'gamesConsoles',
				labelEn: 'Games and Consoles',
				parent: electronics._id,
				slug: 'games-consoles',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'ipods',
				labelEn: 'IPods',
				parent: electronics._id,
				slug: 'ipods',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'airConditioners',
				labelEn: 'Air-Conditioners',
				parent: electronics._id,
				slug: 'air-conditioners',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'cameras',
				labelEn: 'Cameras',
				parent: electronics._id,
				slug: 'cameras',
				ancestors: [
					{
						_id: electronics._id,
						slug: electronics.slug,
						name: electronics.name
					}
				]				
			},
			{
				name: 'clothingAndApparel',
				labelEn: 'Clothing and Apparel',
				parent: fashion._id,
				slug: 'clothing-apparel',
				ancestors: [
					{
						_id: fashion._id,
						slug: fashion.slug,
						name: fashion.name
					}
				]				
			},
			{
				name: 'fashionAccessories',
				labelEn: 'Accessories',
				parent: fashion._id,
				slug: 'fashion-accessories',
				ancestors: [
					{
						_id: fashion._id,
						slug: fashion.slug,
						name: fashion.name
					}
				]				
			},
			{
				name: 'fashionOther',
				labelEn: 'Other',
				parent: fashion._id,
				slug: 'fashion-other',
				ancestors: [
					{
						_id: fashion._id,
						slug: fashion.slug,
						name: fashion.name
					}
				]				
			}	
		].forEach(function(category) {
			AdCategories.insert(category);
		});
	}
});

// var build_ancestors = function(_id, parent_id) {
// 	var parent = db.Categories.findOne(
// 		{'_id': parent_id},
// 		{'name': 1, 'slug': 1, 'ancestors': 1}
// 	);

// 	parent_ancestors = parent.pop('ancestors');
// }