Template.categories.helpers({	
	// Returns the master categories (ie, those which do no have a parent)
	masterCategories: function() {
		return AdCategories.find({parent: { $exists: false }});
	},
	subCategories: function() {
		if(Session.get('selectedMasterCategory')) {
			var subCategories = AdCategories.find({parent: Session.get('selectedMasterCategory')});
			return subCategories;
		}
	},
	masterCategorySelected: function() {
		return Session.get('selectedMasterCategory');
	},
	locationParam: function() {
		return Router.current().params.location;
	}
});

Template.categories.events({
	'click #all': function(e, tpl) {
		Session.set('selectedMasterCategory', '');
	},
	'click .category': function(e, tpl) {
		Session.set('selectedMasterCategory', this._id);
		// console.log(Session.get('selectedMasterCategory'));
	}
});