Template.createProfile.helpers({
	// the currently logged in user (used to populate form with current values)
	current: function() {
		return Meteor.user().profile;
	},
	// return users schema (used to generate form)
	userProfileSchema: function() {
		return Schemas.UserProfile;
	}
});

// Template.createProfile.events({
// 	'click #saveChanges': function(event, tpl) {
// 		event.preventDefault();
// 		// Router.go('profile', Meteor.userId());
// 	}
// });
