Template.profile.rendered = function() {
	this.$('.rateit').rateit();
}

Template.profile.helpers({
	ownProfile: function() {
		return this.profileData._id === Meteor.userId();
	},
	otherProfile: function() {
		return this.profileData._id !== Meteor.userId();
	},
	profileIncomplete: function() {
		var user = Meteor.user();
		if(!user.profile || !user.profile.firstName) {
			return true;
		}
		return false;
	},
	createProfileUrl: function() {
		return Meteor.absoluteUrl() + "create_profile/" + Meteor.userId();
	},
	reviewsData: function() {
		return Reviews.find();
	},
	hasReviews: function() {
		return Reviews.find().count();
	}
});

Template.profile.events({
	'change #fileInput': function(event, tpl) {
		FS.Utility.eachFile(event, function(file){
			var fileObj = new FS.File(file);

			fileObj.metadata = {owner: Meteor.userId()};
			ProfileImageUploads.insert(fileObj, function(err, fileObj) {
				if(err) {
					toastr.error("\u2022 Image size cannot exceed 10 MB. <br/> \u2022 The following file extensions are accepted: .JPG, .JPEG, .PNG", "Upload Error");
				} else {
					toastr.success("File uploaded successfully");
				}
			});
		});
	}
});