Template.home.helpers({
	cities: function() {
		return Cities.find();
	}
});

Template.home.events({
	'submit .set-location': function(e, tpl) {
		e.preventDefault();
		var cityId = tpl.$("#city").val();
		Session.setPersistent('locationId', cityId);
		Router.go('search', {location: cityId});
	}
});