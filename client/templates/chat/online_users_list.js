Template.onlineUsersList.helpers({
	'onlineUsers': function() {
		return Meteor.users.find({ _id: {$ne: Meteor.userId()} });
	}
});

Template.onlineUsersList.events({
	'click .user': function() {
		Session.set('currentId', this._id);
		// returns the room between these 2 users if it already exists.
		var result = ChatRooms.findOne({chatIds: {$all: [this._id, Meteor.userId()]}});
		if(result) {
			// room already exists
			Session.set("roomId", result._id);
		} else {
			// no room exists
			var newRoom = ChatRooms.insert({chatIds:[this._id, Meteor.userId()], messages:[]});
			Session.set("roomId", newRoom);
		}	
	}
});