Template.messages.helpers({
	'messages': function() {
		var result = ChatRooms.findOne({_id: Session.get('roomId')});
		if(result) {
			return result.messages;
		}
		return false;
	}
});

// Template.messages.helpers({
// 	'messages': function() {
// 		return ['a', 'b', '']
// 	}
// });