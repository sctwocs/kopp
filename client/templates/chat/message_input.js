Template.messageInput.events({
	'keydown input#message' : function(event) {
		if(event.which === 13) {
			if(Meteor.user()) {
				var name = Meteor.user().emails[0].address;
				var message = document.getElementById('message');

				if(message.value !== '') {
					var de = ChatRooms.update({"_id": Session.get("roomId")}, {$push: {messages: {
						name: name,
						text: message.value,
						createdAt: Date.now()
					}}});
					// console.log(de);
					document.getElementById('message').value = '';
					message.value = '';
				}
			}
			else {
				alert("You must be logged in to chat.");
			}
		}
	}
})