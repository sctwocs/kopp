Template.setLocation.helpers({
	cities: function() {
		return Cities.find();
	},
	city: function() {
		if(Session.get('locationId')) {
			var city = Cities.findOne({slug: Session.get('locationId')}, {fields: {name: 1}});
			return city.name;
		}
		return;
	},
	isUpdatingLocation: function() {
		return Session.get('isUpdatingLocation');
	}
});


Template.search.events({
	'click a.update-location': function(e, tpl) {
		e.preventDefault();
		Session.set('isUpdatingLocation', true);

	},
	'click a.cancel': function(e, tpl) {
		e.preventDefault();
		Session.set('isUpdatingLocation', false);
	},
	'submit form.set-location': function(e, tpl) {
		e.preventDefault();
		var cityId = tpl.$("#city").val();	// tpl limits the scope of this selector to this template
		Session.setPersistent('locationId', cityId);
		// if the category is set in the url params route to search/:location/:category, 
		// else, route to search/:location
		Router.current().params.category ? Router.go('searchByCategory', {location: cityId, category: Router.current().params.category}) : Router.go('search', {location: cityId});
		Session.set('isUpdatingLocation', false);
	},
});