Template.navigation.helpers({
	// the url to the currently logged in user's profile
	profileUrl: function() {
		return Meteor.absoluteUrl() + "profile/" + Meteor.userId();
	}
});