Meteor.startup(function() {
	// Doesn't seem to work with findOne, has to be explicitly set for now.
	Session.setDefaultPersistent('locationId', 'bandalungwa');
});

// Returns the current location slug from the route params.
var currentLocationParam = function() {
	return Router.current().params.location;
}

// Returns true if a category param exists in route params.
var categoryParamExists = function() {
	if(Router.current().params.category) {
		return true;
	}
	return false;
}

// Returns true if a search query exists (has a truthy value).
var searchQueryExists = function() {
	if(Router.current().params.query.search) {
		return true;
	}
	return false;
}

Template.search.helpers({
	/* The publish function determines which records should be synced to the mini-mongo database of any subscribing clients. So sorting the data in the publish 
	function actually has no effect on the client. As such, we perform the sort operation below. */
	ads: function() {
		// if we are searching (ie, search query exists, sort by score)
		if(Router.current().params.query.search) {
			return Ads.find({}, {sort: {score: -1} });
		}
		// otherwise sort by date posted
		return Ads.find({}, {sort: {postedOn: -1} });
	},
	masterCategories: function() {
		return AdCategories.find({parent: { $exists: false }});
	},
	locationParam: currentLocationParam,
	categoryExists: categoryParamExists,
	currentCategory: function() {
		if(categoryParamExists()){
			return AdCategories.findOne({slug: Router.current().params.category});
		}
		return null;
	},
	isSearching: searchQueryExists,
	searchCount: function() {
		if(searchQueryExists()) {
			return Ads.find().count();
		}
		return null;
	}

});

Template.search.events({
	'submit .keyword-search': function(e, tpl) {
		e.preventDefault();
		var searchQuery = $(e.target).find('[name=search-term]').val();
		var category = $(e.target).find('[name=search-category]').val();

		var location = currentLocationParam();

		// Do this server side eventually
		if(!searchQuery) {
			toastr.error('Search field cannot be empty!');
		}
		// if no cateogry is set, route user to top-level location route (/search/:location)
		else if(!category || category == 'all') {
			Router.go('search', {location: location}, {query: {search: searchQuery} });
		} else {
			Router.go('searchByCategory', {location: location, category: category}, {query: {search: searchQuery} });
		}
	}	
});

