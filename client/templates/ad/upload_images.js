Template.uploadImages.helpers({
	uploads: function() {
		return Uploads.find();
	}
});

Template.uploadImages.events({
	'change .fileInput': function(event, tpl) {
		FS.Utility.eachFile(event, function(file){
			var fileObj = new FS.File(file);
			// var ad = Ads.findOne({_id: tpl.data._id});
			// console.log(ad);
			fileObj.metadata = {owner: Meteor.userId(), adId: tpl.data._id};
			Uploads.insert(fileObj, function(err, fileObj) {
				if(err) {
					console.log(err);
					toastr.error("\u2022 Image size cannot exceed 10 MB. <br/> \u2022 The following file extensions are accepted: .JPG, .JPEG, .PNG", "Upload Error");
				} else {
					// Meteor.call('updateImageReferences', tpl.data._id, fileObj);
					toastr.success("File uploaded successfully");
				}
			});
		});
	},
	'click .removeFile': function(event, tpl) {
		event.preventDefault();
		var message = confirm("Are you sure you want to delete this image?");
		if(message === true) {
			Uploads.remove({_id:this._id}, function(err, result) {
				if(!err) {
					toastr.success("File removed successfully");
				}
			});
		} else {
			toastr.warning("Nothing was removed");
		}
	},
	'click #adPreview': function(event, tpl) {
		event.preventDefault();
		console.log(tpl.data);
		Router.go('adPreview', {_id: tpl.data._id});
	} 
});

// BootstrapModalPrompt.prompt({
//     title: "Confirm something",
//     content: "Do you really want to confirm whatever?"
// }, function(result) {
//   if (result) {
//     // User confirmed it, so go do something.
//   }
//   else {
//     // User did not confirm, do nothing.
//   }
// });


// event.preventDefault();
// var message = confirm("Are you sure you want to delete this image?";
// if(message === true) {
// 	Uploads.remove({_id:this._id}, function(err, result) {
// 		if(!err) {
// 			toastr.success("File removed successfully");
// 		}
// 	});
// } else {
// 	toastr.warning("Nothing was removed");
// }