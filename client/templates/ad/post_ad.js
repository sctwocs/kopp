AutoForm.hooks({
	generalAdInfo: {
		onSuccess: function(doc) {
			Router.go('uploadImages', {_id: this.docId});
		}
	}
});	

Template.postAd.rendered = function() {
	initEditor();
};

function initEditor() {
	$('.editor').summernote({
		// summer note options
		height: 300,
  		minHeight: null,             // set minimum height of editor
  		maxHeight: null,             // set maximum height of editor
	});
};

Template.postAd.helpers({
	showSubCategorySelect: function() {
		var value = AutoForm.getFieldValue('masterCategory', 'generalAdInfo');
		if(value) {
			return true;
		}
		return false;
	}
});