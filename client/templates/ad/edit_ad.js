Template.editAd.rendered = function() {
	initEditor();
};

function initEditor() {
	$('.editor').summernote({
		// summer note options
		height: 300,
  		minHeight: null,             // set minimum height of editor
  		maxHeight: null,             // set maximum height of editor
	});
};

Template.editAd.helpers({
});