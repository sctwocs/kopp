Template.adManager.helpers({
	hasAds: function() {
		return Ads.find().count();
	}
});

Template.adManager.events({
	'click .edit': function(e, tpl) {
		e.preventDefault();

		Router.go('editAd', {_id: this._id});
	},
	'click .delete': function(e, tpl) {
		e.preventDefault();

		// for testing, create a server-side meteor method for this later that will also delete corresponding image from the
		// store.
		if(confirm("Delete this ad?")) {
			Meteor.call("deleteAd", this._id);
		}

	},
	'click .upgrade': function(e, tpl) {
		e.preventDefault;

		// Router.go('upgradeAd', this);
	}
});