Template.reviewInputPanel.events({
	'submit form': function(e, tpl) {
		e.preventDefault();

		var review = {
			 rating: tpl.$('#add-rating').rateit('value'),
			 comment: tpl.$(e.target).find('[name=comment]').val(),
			 postedTo: tpl.data.profileData._id
		};

		Meteor.call('addReview', review, function(error, result){
			if(error) {
				return alert(error.reason);
			} else {
				tpl.$(".panel-body").html(function() {
					return "<p> Your review has been submitted. </p>";
				});
			}
		});


	}
});