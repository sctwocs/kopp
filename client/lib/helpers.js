Template.registerHelper("stripHtml", function(html) {
	return html.replace(/(<([^>]+)>)/ig,"");
});

Template.registerHelper('formatDate', function(date) {
	return moment(date).format('MM-DD-YYYY');
});

Template.registerHelper('formatDate2', function(date) {
	return moment(date).format('MMMM Do YYYY');
});

Template.registerHelper('relativeTime', function(date) {
	return moment(date).fromNow();
});

Template.registerHelper('formatMoney', function(amount) {
	return accounting.formatMoney(amount);
});

Template.registerHelper('capitalize', function(str){
	return str.toUpperCase();
});

Template.registerHelper('getProfileURL', function(userId) {
	return Meteor.absoluteUrl() + "profile/" + userId;
});