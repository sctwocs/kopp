Meteor.startup(function() {
	// Grab our testPublishableKey from our settings.json file. Note: we're pulling
	// this from our public block where we stored values that are safe to expose on the 
	// client.
	var stripeKey = Meteor.settings.public.stripe.testPublishableKey;
	// We pass that value to a new method given to us by Stripe.js.
	// This let's Stripe know about us and associates our activity on the client
	// with our Stripe account.
	Stripe.setPublishableKey(stripeKey);

	// Helper methods for handling token creation
	// Ensure PCI compliance: PCI DSS (Payment Card Industry Data Security Standard)
	// Set of requirements designed to ensure that all companies that process, store, or transmit,
	// credit card information maintain a secure environment.
	// We need to pass our user's card data to Stripe first in order to create a card token.
	STRIPE = {
		// Responsible for taking our user's card data and passing it to Stripe. Depending on the
		// response we get from Stripe, we either throw an error. Otherwise, we pass the data along to
		// our setToken method.
		// We'll be reusing this method on a few different forms, so this allows
		// us to get the form element's DOM selector and a function to call
		// after stripe has returned a token (as callback)
		getToken: function(domElement, card, callback) {
			Stripe.card.createToken(card, function(status, response) {
				if (response.error) {
					toastr.error(response.error.message, "Error");
				} else {
					STRIPE.setToken(response.id, domElement, callback);
				}
			});
		},
		setToken: function(token, domElement, callback) {
			$(domElement).append( $( "<input type='hidden' name='stripeToken' />" ).val(token));
      		callback();
		}
	}
});